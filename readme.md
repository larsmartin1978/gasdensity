# Gas density calculator
## Motivation
Working with gas-based particle detectors, it is quite common to have to calculate the density for arbitrary gas mixtures, especially for input to [SRIM](http://www.srim.org/) or similar programs. While this is easy to do by hand, it offers several opportunities to mess up.
For this reason, and as Puthon practice I wrote this little helper program.

## Basic Usage and Examples
The usage of this code is very simple:

```
gasdensity [-p <pressure in mbar>] [-t <temp in Celsius>] [-u <desired density unit>] <gas1> <factor1> <gas2> <factor2> [<gas3> <factor3>...]
```

The gases have to be given as standard chemical formulas, the factors only need to represent the ratio of the components, so a common possibility would be to use percentages:

```
gasdensity Ar 90 CO2 10
```

but it is not required for the numbers to add up to 100, or 1, or any specific value, so this is also a valid call:
```
gasdensity He 7 CH4 11
```

## Pressure and Temperature
The program accepts optional pressure and temperature parameters, if none are given, NTP is assumed, i.e. 20°C = 293.15K and 1013.25 mbar. E.g.:
```
gasdensity -t 40 -p 200 He 90 CO2 10
```
will report the density of a 90:10 He/CO2 mix at 200mbar and 40°C. Currently only those units are accepted for these parameters, but since the code already uses [Pint](https://pint.readthedocs.io/) it would be easy to include the option of different units.

## Output units
By default the gas density will be given in units of g/cm^3, but through the magic of [Pint](https://pint.readthedocs.io/) any output unit can be requested, provided it is indeed a mass density unit. E.g.:
```
gasdensity -t 50 -u "kg/m**3" He 70 CO2 30
```
but also these:
```
gasdensity -t 50 -u "amu/parsec^3" He 70 CO2 30
gasdensity -t 50 -u "carat/hogshead" He 70 CO2 30
```

Note that both `^` and `**` are supported for the exponent.

## Under the Hood
The element masses are taken from the module [Periodic](https://pypi.python.org/pypi/periodic), and [Pint](https://pint.readthedocs.io/) is used for the unit conversion. For the density calculation an ideal gas is assumed, which means one mole of gas at NTP takes up a volume of 24.0548l = 24054.8cm^3, proportional to the temperature in K and inversely proportional to the pressure.

## Caveats/Bugs
The program does not (and cannot) check whether the components you give it are gaseous at the given temperature and pressure. Furthermore even if they are, it needs to be considered whether the ideal gas approximation is valid for them.