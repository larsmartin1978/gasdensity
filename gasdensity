#!/usr/bin/python
# Author: Lars Martin https://gitlab.com/larsmartin1978
# Date: 12 Oct. 2017

from periodic.table import element
import sys,getopt
import pint
import re

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def usage():
    print "Usage: gasdensity [-p <pressure in mbar>] [-t <temp in Celsius>] [-u <desired density unit>] <gas1> <factor1> <gas2> <factor2> [<gas3> <factor3>...]"

def main(argv):
    opts, args = getopt.getopt(argv,"ht:p:u:",["help","temp=","pressure=","unit="])
    if( len(args) < 4):
        usage()
        sys.exit();

    ureg = pint.UnitRegistry()
    Q_ = ureg.Quantity

    constituents = []
    totFact = 0.
    tempNTP = 293.15 *ureg.kelvin
    pNTP = 1013.25 *ureg.millibar
    temp = tempNTP
    p = pNTP
    unit = "g/cm**3"

    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-t", "--temp"):
            temp = Q_(float(a), ureg.degC)
            temp = temp.to('kelvin')
            print temp
        elif o in ("-p", "--pressure"):
            p = float(a) *ureg.millibar
            print p
        elif o in ("-u", "--unit"):
            unit = a.replace("^","**") # convert exponent to pythonic way
        else:
            assert False, "unhandled option"

    for a in args:
        if(is_number(a)):
            totFact += float(a)

    for i in range(0,len(args),2):
        el = element(args[i])
        fact = float(args[i+1])
        if(el):                 # Element found
            constituents.append([el,fact])
        else:                   # try to deconstruct molecule
            # print args[i]
            atoms = re.findall(r'[A-Z][a-z]?[0-9]*', args[i]) # Split chemical formula into things that look like <element><number>, e.g. CO2 -> [ C, O2 ]
            # print atoms
            totAtoms = 0.
            molConst = []
            for atom in atoms:
                stoch = re.findall(r'\d+', atom) # find stoichiometric number
                if(stoch):
                    stoch = float(stoch[0])
                    atom = re.findall(r'[A-Z][a-z]?', atom)[0] # find element symbol
                else:
                    stoch = 1.0
                el = element(atom)
                if(el):
                    molConst.append([el,stoch])
                else:
                    print "Could not find element", atom, "in molecule", args[i]
                    sys.exit()

            for constit in molConst:
                constituents.append([constit[0], constit[1]*fact])

    # print constituents

    totDens = 0.
    for c in constituents:
        # print c[1], '/',totFact,'*',c[0].mass,'/24054.8' # molar volume at NTP
        partDens = c[1]/totFact * c[0].mass*ureg.g/(24054.8 *ureg.cc * temp/tempNTP * pNTP/p) # molar volume at NTP
        totDens += partDens

    print totDens.to(unit)

if __name__ == "__main__":
   main(sys.argv[1:])
